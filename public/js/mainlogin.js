var Fn = {
	// Valida el rut con su cadena completa "XXXXXXXX-X"
	validaRut : function (rutCompleto) {
		rutCompleto = rutCompleto.replace("‐","-");
		if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test( rutCompleto ))
			return false;
		var tmp 	= rutCompleto.split('-');
		var digv	= tmp[1]; 
		var rut 	= tmp[0];
		if ( digv == 'K' ) digv = 'k' ;
		
		return (Fn.dv(rut) == digv );
	},
	dv : function(T){
		var M=0,S=1;
		for(;T;T=Math.floor(T/10))
			S=(S+T%10*(9-M++%6))%11;
		return S?S-1:'k';
	}
}

function ingresar() {
  //var form = document.getElementById("ingreso_form");
  var rut = document.getElementById("rut_id").value; 
  var pass = document.getElementById("pass_id").value; 
 
  // espacio en blanco

  if (validar( rut )){
  //alert("El rut ingresado es válido");
} else {
      
      document.getElementById("rut_id").focus();
      Swal.fire('RUT','Debe ingresar Rut','error');
      return false;
}

  // 4 caracteres 

  if (validar_min( rut )){
  //alert("aca");
} else {
      document.getElementById("rut_id").focus();
      Swal.fire('RUT','El campo debe contener al menos 4 carácteres','error');
      return false;
}

  // validar rut

  if (Fn.validaRut( rut )){
  
} else {
  
      document.getElementById("rut_id").focus();
      Swal.fire('RUT','El Rut no es válido','error');
      return false;
}

  // espacio en blanco

  if (validar( pass )){
  //alert("El rut ingresado es válido");
} else {
      document.getElementById("pass_id").focus();
  Swal.fire('PASSWORD','Debe ingresar Password','error');
      return false;
}
  if (validar_min( pass )){
  //alert("aca");
} else {
      document.getElementById("pass_id").focus();
      Swal.fire('PASSWORD','El campo debe contener al menos 4 carácteres','error');
      return false;
}
//form.submit();
//alert("lol");
}

var form = document.getElementById("ingreso_form");

/*document.getElementById("btn_ingreso").addEventListener("click", function () {});*/
function input_letras(e) {
    var key = e.keyCode || e.which,
        tecla = String.fromCharCode(key).toLowerCase(),
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyz",
        especiales = [8, 37, 39, 46],
        tecla_especial = false;

    for (var i in especiales) {
        if (key == especiales[i]) {
        tecla_especial = true;
        break;
        }
    }
    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
    }
}
function input_letras_num(e) {
    var patt = new RegExp(/^[A-Za-z0-9\s]+$/g);
    var res = patt.test(e);
    return res;
}
function registrar() {
    var form_r = document.getElementById("registro_form");
    var nombre_r = document.getElementById("id_nombre").value; 
    var apellido_r = document.getElementById("id_apellido").value; 
    var rut_r = document.getElementById("id_rut").value; 
    var pass_r = document.getElementById("id_clave").value; 
    var pass1_r = document.getElementById("id_clave2").value; 
    
    // Validar Input Nombre

    if (validar( nombre_r )){
		//alert("El rut ingresado es válido");
	} else {
        document.getElementById("id_nombre").focus();
        Swal.fire('NOMBRE','Debe ingresar Nombre','error');
		//alert("Debe ingresar Nombre");   
        return false;
	}
    if (validar_min( nombre_r )){
		//alert("aca");
	} else {
        Swal.fire('NOMBRE','El campo debe contener al menos 4 carácteres','error');
        document.getElementById("id_nombre").focus();
        return false;
	}

    // Validar Input Apellido

    if (validar( apellido_r )){
		//alert("El rut ingresado es válido");
	} else {
		//alert("Debe ingresar Apel");
        document.getElementById("id_apellido").focus();
        Swal.fire('APELLIDO','Debe ingresar Apellido','error'); 
        return false;
	}
    if (validar_min( apellido_r )){
		//alert("aca");
	} else {
        Swal.fire('APELLIDO','El campo debe contener al menos 4 carácteres','error');
        document.getElementById("id_apellido").focus();
        return false;
	}

    // Validar Input rut

    if (validar( rut_r )){
		//alert("El rut ingresado es válido");
	} else {
        document.getElementById("id_rut").focus();
        Swal.fire('RUT','Debe ingresar RUT','error');
		//alert("Debe ingresar Rut");
        return false;
	}
    if (validar_min( rut_r )){
		//alert("aca");
	} else {
        Swal.fire('RUT','El campo debe contener al menos 4 carácteres','error');
        document.getElementById("id_rut").focus();
        return false;
	}
      // validar rut
      if (Fn.validaRut( rut_r )){
		//$("#msgerror").html("El rut ingresado es válido");
	} else {
		//$("#msgerror").html("El Rut no es válido");
        document.getElementById("id_rut").focus();
        Swal.fire('RUT','El Rut no es válido','error');
        return false;
	}

    // Validar Input Password

    if (validar( pass_r )){
		//alert("El rut ingresado es válido");
	} else {
        document.getElementById("id_clave").focus();
        Swal.fire('PASSWORD','Debe ingresar Password','error');
		//alert("Debe ingresar Password");
        return false;
	}
    if (validar_min( pass_r )){
		//alert("aca");
	} else {
        document.getElementById("id_clave").focus();
        Swal.fire('PASSWORD','El campo debe contener al menos 4 carácteres','error');
        return false;
	}

    // Validar Input Password 2

    if (validar( pass1_r )){
		//alert("El rut ingresado es válido");
	} else {
        document.getElementById("id_clave2").focus();
        Swal.fire('CONFIRMAR PASSWORD','Debe ingresar Confirmar Password','error');
		//alert("Debe Confirmar Password");
        //document.getElementById("rut_id").focus();
        return false;
	}
    if (validar_min( pass1_r )){
		//alert("aca");
	} else {
        document.getElementById("id_clave2").focus();
        Swal.fire('CONFIRMAR PASSWORD','El campo debe contener al menos 4 carácteres','error');
        return false;
	}

    // validar password

    if (pass_r != pass1_r) {
        alert("Las passwords deben de coincidir");
        return false;
      } else {
        alert("Todo esta correcto");
        return true; 
      }

    //form_r.submit();
}
function validar(str) {
    if (str.length == 0) {
      //alert('Ingrese rut');
      return false;
    }else{
        return true;
    }
  }
  function validar_min(str) {
    if (str.length < 4) {
      //alert('Ingrese rut');
      return false;
    }else{
        return true;
    }
  }