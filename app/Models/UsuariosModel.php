<?php 

namespace App\Models;
use CodeIgniter\Model;

// Define nombre de la tabla
define('TABLE', 'usuarios');

class UsuariosModel extends Model {
	
	// Obtiene todos los registros
	public function all() {
		$db = \Config\Database::connect();
		
		$result = $db->table(TABLE);
		$result->select('usuarios.id, usuarios.nombre, usuarios.apellido, usuarios.rut, tipos.tipo, empresas.nombre_empresa');
		$result->join('tipos', 'tipos.id = usuarios.tipo', 'left');
		$result->join('empresas', 'empresas.id_empresa = usuarios.empresa', 'left');
	
		return $result->get()->getResult();
	}
	
	// Obtiene un registro por ID
	public function one($where) {
		$db = \Config\Database::connect();
					
		$result = $db->table(TABLE);
		$result->where($where);
		
		return $result->get()->getResultArray();	
	}
	
	// Ingresa un nuevo registro
	public function _create($data) {
		$db = \Config\Database::connect();
			
		$result = $db->table(TABLE);
		$result->insert($data);
		
		return $db->insertID(); 
	}
	
	// Actualiza un nuevo registro
	public function _update($where, $data) {
		$db = \Config\Database::connect();
			
		$result = $db->table(TABLE);
		$result->set($data);
		$result->where($where);
		
		return $result->update(); 
	}
	
	// Elimina un registro
	public function _delete($data) {
		$db = \Config\Database::connect();
			
		$result = $db->table(TABLE);
		$result->where($data);
		
		return $result->delete();
	}
	
	

	
	
}