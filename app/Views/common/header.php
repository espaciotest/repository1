<div id="page">
	
<header class="header menu_2">
	<div id="preloader"><div data-loader="circle-side"></div></div><!-- /Preload -->
	<div id="logo"> <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>/img/logo.png" width="288" height="63" data-retina="true" alt=""></a> </div>
	<ul id="top_menu">
		<li class="hidden_tablet"><a href="<?php echo base_url('/login'); ?>" class="btn_1 rounded">Iniciar sesión</a></li>
		<li class="hidden_tablet"><a href="<?php echo base_url('/registrar'); ?>" class="btn_1 rounded">Registrarse</a></li>
		<li class="hidden_tablet"><img src="<?php echo base_url(); ?>/img/logo-duoc.png" width="250" height="60" data-retina="true" alt="Duoc"></li>
	</ul>
	<!-- /top_menu -->
	<a href="#menu" class="btn_mobile">
		<div class="hamburger hamburger--spin" id="hamburger">
			<div class="hamburger-box">
				<div class="hamburger-inner"></div>
			</div>
		</div>
	</a>
	<nav id="menu" class="main-menu">
		<ul>
			<li><span><a href="<?php echo base_url(); ?>">Inicio</a></span>
			</li>
			<li><span><a href="<?php echo base_url('/contacto'); ?>">Contacto</a></span></li>
		</ul>
	</nav>
	<!-- Search Menu -->
	<div class="search-overlay-menu">
		<span class="search-overlay-close"><span class="closebt"><i class="ti-close"></i></span></span>
		<form role="search" id="searchform" method="get">
			<input value="" name="q" type="search" placeholder="Search..." />
			<button type="submit"><i class="icon_search"></i>
			</button>
		</form>
	</div><!-- End Search Menu -->
</header>
<!-- /header -->