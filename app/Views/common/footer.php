<footer class="revealed">
	<div class="container margin_default">
	  <div class="row">
		<div class="col-lg-5 col-md-12 p-r-5">
		  <p><img src="<?php echo base_url(); ?>/img/logo.png" width="192" height="42" data-retina="true" alt=""></p>
		  <p>Plataforma de Escuela de Prevención de Riesgos.</p>
		</div>
		<div class="col-lg-3 col-md-6">
		  <h5>Contáctanos</h5>
		  <ul class="contacts">
			<li><a href="tel:+56912345678"><i class="ti-mobile"></i> + 56912345678</a></li>
			<li><a href="mailto:info@flexworking.cl"><i class="ti-email"></i> info@flexworking.cl</a></li>
		  </ul>
		</div>
	  </div>
	  <!--/row-->
	  <hr>
	  <div class="row">
		<div class="col-md-8">
		  <ul id="additional_links">
			<li><a href="#0">Términos y condiciones</a></li>
			<li><a href="#0">Privacidad</a></li>
		  </ul>
		</div>
		<div class="col-md-4">
		  <div id="copy">© 2021 Flexworking</div>
		</div>
	  </div>
	</div>
  </footer>
	<!--/footer-->
	</div>
	<!-- page -->
	
	<!-- COMMON SCRIPTS -->
  <script src="<?php echo base_url(); ?>/js/jquery-2.2.4.min.js"></script>
  <script src="<?php echo base_url(); ?>/js/common_scripts.js"></script>
  <script src="<?php echo base_url(); ?>/js/main.js"></script>
  <script src="<?php echo base_url(); ?>/assets/validate.js"></script>
		
	<!-- SPECIFIC SCRIPTS -->
	<script src="<?php echo base_url(); ?>/js/video_header.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.6/dist/sweetalert2.all.min.js"></script>
	<script>
	
		let result = '<?php echo $result; ?>';
		if (result == '1') {
			Swal.fire(
			  'Correcto!',
			  'Cambios realizados con exito!',
			  'success'
			)
		} else if (result == '0') {
			Swal.fire(
			  'Error!',
			  'Hubo un error!',
			  'success'
			)
		}
	
		HeaderVideo.init({
			container: $('.header-video'),
			header: $('.header-video--media'),
			videoTrigger: $("#video-trigger"),
			autoPlayVideo: true
		});
		
		
	</script>

</body>
</html>