<main>

<section id="hero_in" class="general">
  <div class="wrapper">
	<div class="container">
	  <h1 class="fadeInUp"><span></span>Usuarios</h1>
	</div>
  </div>
</section>
<div class="logo-empresa"><img src="<?php echo base_url(); ?>/img/logo-empresa.jpg" alt=""></div>

<div class="container margin_default">
	<div class="main_title_2">
		<span><em></em></span>
		<h2>Nuevo Usuario</h2>
		<p>Agregar nuevo usuario.</p>
	</div>
	
	<form action="<?php echo base_url('/usuarios/crear');?>" method="post">
		<div class="mb-3">
			<label for="nombre" class="form-label">Nombre</label>
			<input type="text" class="form-control" id="nombre" name="nombre"  required>
		</div>
		<div class="mb-3">
			<label for="apellido" class="form-label">Apellido</label>
			<input type="text" class="form-control" id="apellido" name="apellido"  required>
		</div>
		<div class="mb-3">
			<label for="rut" class="form-label">RUT</label>
			<input type="text" class="form-control" id="rut" name="rut"  required>
		</div>
		<div class="mb-3">
			<label for="email" class="form-label">Email</label>
			<input type="email" class="form-control" id="email" name="email"  required>
		</div>
		<div class="mb-3">
			<label for="fono" class="form-label">Fono</label>
			<input type="text" class="form-control" id="fono" name="fono" required>
		</div>
		<div class="mb-3">
			<label for="empresa" class="form-label">Empresa</label>
			<input type="text" class="form-control" id="empresa" name="empresa"  required>
		</div>
		<div class="mb-3">
			<label for="clave" class="form-label">Contraseña</label>
			<input type="text" class="form-control" id="clave" name="clave" required>
		</div>
		
		<div class="mb-3">
			<label for="tipo" class="form-label">Tipo</label>
			<select class="form-select form-select-md mb-3" aria-label=".form-select-md example" name="tipo" id="tipo">
				<option selected>Seleccione</option>
				<option value="1">Administrador</option>
				<option value="2">Empresa</option>
				<option value="3">Profesor</option>
				<option value="4">Alumno</option>
				<option value="5">Trabajador</option>
			</select>
		</div>

	  
	  <p class="text-center">
		  <button type="submit" class="btn_1 rounded">Crear Nuevo Usuario</button>&nbsp;&nbsp;<a href="<?php echo base_url('usuarios/listado'); ?>" class="btn_1 rounded" style="background:#cc0000;">Cancelar</a>
	  </p>
	</form>

</div>

</main>