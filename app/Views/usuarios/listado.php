<main>

<section id="hero_in" class="general">
  <div class="wrapper">
	<div class="container">
	  <h1 class="fadeInUp"><span></span>Usuarios</h1>
	</div>
  </div>
</section>
<div class="logo-empresa"><img src="<?php echo base_url(); ?>/img/logo-empresa.jpg" alt=""></div>

<div class="container margin_default">
	
	<div class="main_title_2">
		<span><em></em></span>
		<h2>Usuarios</h2>
		<p>Listado de usuarios.</p>
	</div>
	<?php echo session('rut'); ?>
	<p class="text-center">
		<a href="<?php echo base_url('usuarios/nuevo'); ?>" class="btn_1 rounded">Crear Nuevo Usuario</a>&nbsp;&nbsp;<a href="#" class="btn_1 rounded">Carga Masiva</a>
	</p>
	
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th>Usuario</th>
				<th>RUT</th>
				<th>Tipo</th>
				<th>Empresa</th>
				<th>Editar</th>
				<th>Borrar</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($datos as $key) : ?>
			<tr>
				<td><?php echo $key->nombre; ?> <?php echo $key->apellido; ?></td>
				<td><?php echo $key->rut; ?></td>
				<td><?php echo $key->tipo; ?></td>
				<td><?php echo $key->nombre_empresa; ?></td>
				<td><a href="<?php echo base_url('/usuarios/editar/'.$key->id); ?>" class="btn btn-sm btn-warning">Editar</a></td>
				<td><a href="<?php echo base_url('/usuarios/eliminar/'.$key->id); ?>" class="btn btn-sm btn-danger">Eliminar</a></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

</div>
</main>