<main>

<section id="hero_in" class="general">
  <div class="wrapper">
	<div class="container">
	  <h1 class="fadeInUp"><span></span>Usuarios</h1>
	</div>
  </div>
</section>
<div class="logo-empresa"><img src="<?php echo base_url(); ?>/img/logo-empresa.jpg" alt=""></div>

<div class="container margin_default">
	<div class="main_title_2">
		<span><em></em></span>
		<h2>Editar</h2>
		<p>Editar usuario.</p>
	</div>
<?php echo "<pre>"; print_r($datos); echo "</pre>";?>
	<form action="<?php echo base_url('/usuarios/edita');?>" method="post">
		<div class="mb-3">
			<label for="nombre" class="form-label">Nombre</label>
			<input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $datos[0]['nombre'];?>" required>
		</div>
		<div class="mb-3">
			<label for="apellido" class="form-label">Apellido</label>
			<input type="text" class="form-control" id="apellido" name="apellido" value="<?php echo $datos[0]['apellido'];?>" required>
		</div>
		<div class="mb-3">
			<label for="rut" class="form-label">RUT</label>
			<input type="text" class="form-control" id="rut" name="rut" value="<?php echo $datos[0]['rut'];?>" required>
		</div>
		<div class="mb-3">
			<label for="email" class="form-label">Email</label>
			<input type="email" class="form-control" id="email" name="email" value="<?php echo $datos[0]['email'];?>" required>
		</div>
		<div class="mb-3">
			<label for="fono" class="form-label">Fono</label>
			<input type="text" class="form-control" id="fono" name="fono" value="<?php echo $datos[0]['fono'];?>" required>
		</div>
		<div class="mb-3">
			<label for="empresa" class="form-label">Empresa</label>
			<input type="number" class="form-control" id="empresa" name="empresa" value="<?php echo $datos[0]['empresa'];?>" required>
		</div>
		<div class="mb-3">
			<label for="clave" class="form-label">Contraseña</label>
			<input type="text" class="form-control" id="clave" name="clave" placeholder="Mantener contraseña">
		</div>
		
		<div class="mb-3">
			<label for="tipo" class="form-label">Tipo</label>
			<select class="form-select form-select-md mb-3" aria-label=".form-select-md example" name="tipo" id="tipo">
				<option <?php if ($datos[0]['tipo'] == 1) { echo "selected"; } ?> value="1">Administrador</option>
				<option <?php if ($datos[0]['tipo'] == 2) { echo "selected"; } ?> value="2">Empresa</option>
				<option <?php if ($datos[0]['tipo'] == 3) { echo "selected"; } ?> value="3">Profesor</option>
				<option <?php if ($datos[0]['tipo'] == 4) { echo "selected"; } ?> value="4">Alumno</option>
				<option <?php if ($datos[0]['tipo'] == 5) { echo "selected"; } ?> value="5">Trabajador</option>
			</select>
		</div>
		
		<input type="hidden" name="id" value="<?php echo $datos[0]['id'];?>">
		<input type="hidden" name="clave_anterior" value="<?php echo $datos[0]['clave'];?>">
	  <p class="text-center">
			<button type="submit" class="btn_1 rounded">EditarUsuario</button>&nbsp;&nbsp;<a href="<?php echo base_url('usuarios/listado'); ?>" class="btn_1 rounded" style="background:#cc0000;">Cancelar</a>
		</p>
	</form>

</div>

</main>