<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<title>FLEXWORKING | Plataforma Escuela de Prevención de Riesgos</title>

	<!-- Favicons-->
	<link rel="shortcut icon" href="<?php echo base_url(); ?>/img/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" type="image/x-icon" href="<?php echo base_url(); ?>/img/apple-touch-icon-57x57-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo base_url(); ?>/img/apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo base_url(); ?>/img/apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo base_url(); ?>/img/apple-touch-icon-144x144-precomposed.png">

	<!-- BASE CSS -->
	<link href="<?php echo base_url(); ?>/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>/css/style.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>/css/vendors.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>/css/icon_fonts/css/all_icons.min.css" rel="stylesheet">

	<!-- YOUR CUSTOM CSS -->
	<link href="<?php echo base_url(); ?>/css/custom.css" rel="stylesheet">

</head>

<body id="register_bg">
	
	<nav id="menu" class="fake_menu"></nav>
	
	<div id="preloader">
		<div data-loader="circle-side"></div>
	</div>
	<!-- End Preload -->
	
	<div id="login">
		<aside>
			<figure>
				<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>/img/logo.png" width="288" height="63" data-retina="true" alt=""></a>
			</figure>
			<!-- <form action="<?php //echo base_url('/usuarios/registro');?>" method="post" autocomplete="off"> -->
			<form id="registro_form"  autocomplete="off">
				<div class="form-group">

					<span class="input">
					<input class="input_field" type="text" name="nombre" id="id_nombre" onkeypress="return input_letras(event)" maxlength="20" required>
						<label class="input_label">
						<span class="input__label-content">Tu Nombre</span>
					</label>
					</span>

					<span class="input">
					<input class="input_field" type="text" name="apellido" id="id_apellido" onkeypress="return input_letras(event)" maxlength="20" required>
						<label class="input_label">
						<span class="input__label-content">Tu Apellido</span>
					</label>
					</span>

					<span class="input">
					<input class="input_field" type="text" name="rut"  id="id_rut" required>
						<label class="input_label">
						<span class="input__label-content">Tu Rut</span>
					</label>
					</span>

					<span class="input">
					<input class="input_field" type="password" id="id_clave" name="clave" required>
						<label class="input_label">
						<span class="input__label-content">Tu password</span>
					</label>
					</span>

					<span class="input">
					<input class="input_field" type="password" id="id_clave2" name="clave2" required>
						<label class="input_label">
						<span class="input__label-content">Confirmar password</span>
					</label>
					</span>
					<p id="msgerror"></p>
					<div id="pass-info" class="clearfix"></div>
				</div>
				<button type="button" onclick="registrar();"   class="btn_1 rounded full-width add_top_60">Registrar</button>
			<!--	<button type="submit" class="btn_1 rounded full-width add_top_30">Registrar</button> -->
				<div class="text-center add_top_10">Ya tienes una cuenta? <strong><a href="<?php echo base_url('/login'); ?>">Inicia Sesión</a></strong></div>
			</form>
			<div class="copy">© 2021 Flexworking</div>
		</aside>
	</div>
	<!-- /login -->
	
	<!-- COMMON SCRIPTS -->
	<script src="<?php echo base_url(); ?>/js/jquery-2.2.4.min.js"></script>
	<script src="<?php echo base_url(); ?>/js/common_scripts.js"></script>
	<script src="<?php echo base_url(); ?>/js/main.js"></script>
	<script src="<?php echo base_url(); ?>/js/mainlogin.js"></script>
	<script src="<?php echo base_url(); ?>/assets/validate.js"></script>
	
	<!-- SPECIFIC SCRIPTS -->
	<script src="<?php echo base_url(); ?>/js/pw_strenght.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.3.6/dist/sweetalert2.all.min.js"></script>
	<script>
		let result = '<?php echo $result; ?>';
		if (result == '1') {
			Swal.fire(
			  'Correcto!',
			  'Cambios realizados con exito!',
			  'success'
			)
		} else if (result == '0') {
			Swal.fire(
			  'Error!',
			  'Las contraseñas no coinciden!',
			  'success'
			)
		}
	</script>
  
</body>
</html>