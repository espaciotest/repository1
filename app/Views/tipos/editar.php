<main>

<section id="hero_in" class="general">
  <div class="wrapper">
	<div class="container">
	  <h1 class="fadeInUp"><span></span>Tipos de Usuarios</h1>
	</div>
  </div>
</section>
<div class="logo-empresa"><img src="<?php echo base_url(); ?>/img/logo-empresa.jpg" alt=""></div>

<div class="container margin_default">
	<div class="main_title_2">
		<span><em></em></span>
		<h2>Editar</h2>
		<p>Editar tipo de usuario.</p>
	</div>

	<form action="<?php echo base_url('/tipos/edita');?>" method="post">
	  <div class="mb-3">
		  <label for="tipo" class="form-label">Tipo de Usuario</label>
		  <input type="text" class="form-control" id="tipo" name="tipo" value="<?php echo $datos[0]['tipo'];?>" required>
		</div>
	  
	  <input type="hidden" name="id" value="<?php echo $datos[0]['id'];?>">
	  <p class="text-center">
			<button type="submit" class="btn_1 rounded">Editar Tipo de Usuario</button>&nbsp;&nbsp;<a href="<?php echo base_url('tipos/listado'); ?>" class="btn_1 rounded" style="background:#cc0000;">Cancelar</a>
		</p>
	</form>

</div>

</main>