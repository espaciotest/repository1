<main>

<section id="hero_in" class="general">
  <div class="wrapper">
	<div class="container">
	  <h1 class="fadeInUp"><span></span>Tipos de Usuario</h1>
	</div>
  </div>
</section>
<div class="logo-empresa"><img src="<?php echo base_url(); ?>/img/logo-empresa.jpg" alt=""></div>

<div class="container margin_default">
	<div class="main_title_2">
		<span><em></em></span>
		<h2>Nuevo Tipo de Usuario</h2>
		<p>Agregar nueva tipo de usuario.</p>
	</div>
	
	<form action="<?php echo base_url('/tipos/crear');?>" method="post">
	  <div class="mb-3">
		<label for="tipo" class="form-label">Tipo de Usuario</label>
		<input type="text" class="form-control" id="tipo" name="tipo" required>
	  </div>
	  
	  <p class="text-center">
		  <button type="submit" class="btn_1 rounded">Crear Nuevo Tipo de Usuario</button>&nbsp;&nbsp;<a href="<?php echo base_url('tipos/listado'); ?>" class="btn_1 rounded" style="background:#cc0000;">Cancelar</a>
	  </p>
	</form>

</div>

</main>