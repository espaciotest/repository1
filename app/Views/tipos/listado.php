<main>

<section id="hero_in" class="general">
  <div class="wrapper">
	<div class="container">
	  <h1 class="fadeInUp"><span></span>Tipos de Usuarios</h1>
	</div>
  </div>
</section>
<div class="logo-empresa"><img src="<?php echo base_url(); ?>/img/logo-empresa.jpg" alt=""></div>

<div class="container margin_default">
	
	<div class="main_title_2">
		<span><em></em></span>
		<h2>Tipos de Usuarios</h2>
		<p>Listado de tipos de usuarios.</p>
	</div>
	
	
	<p class="text-center">
		<a href="<?php echo base_url('tipos/nuevo'); ?>" class="btn_1 rounded">Crear Nuevo Tipo de Usuario</a>
	</p>
	
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th>ID</th>
				<th>Tipo de Usuario</th>
				<th>Editar</th>
				<th>Eliminar</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($datos as $key) : ?>
			<tr>
				<td><?php echo $key->id; ?></td>
				<td><?php echo $key->tipo; ?></td>
				<td><a href="<?php echo base_url('/tipos/editar/'.$key->id); ?>" class="btn btn-sm btn-warning">Editar</a></td>
				<td><a href="<?php echo base_url('/tipos/eliminar/'.$key->id); ?>" class="btn btn-sm btn-danger">Eliminar</a></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

</div>
</main>