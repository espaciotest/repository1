<main>

<section id="hero_in" class="general">
  <div class="wrapper">
	<div class="container">
	  <h1 class="fadeInUp"><span></span>Videos</h1>
	</div>
  </div>
</section>
<div class="logo-empresa"><img src="<?php echo base_url(); ?>/img/logo-empresa.jpg" alt=""></div>

<div class="container margin_default">
	
	<div class="main_title_2">
		<span><em></em></span>
		<h2>Videos</h2>
		<p>Listado de videos.</p>
	</div>
	
	
	<p class="text-center">
		<a href="<?php echo base_url('videos/nuevo'); ?>" class="btn_1 rounded">Crear Nuevo Video</a>
	</p>
	
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th>Tipo de Usuario</th>
				<th>Título</th>
				<th>URL Video</th>
				<th>Imagen Video</th>
				<th>Orden</th>
				<th>Editar</th>
				<th>Eliminar</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($datos as $key) : ?>
			<tr>
				<td><?php echo $key->id_tipo; ?></td>
				<td><?php echo $key->titulo_video; ?></td>
				<td><?php echo $key->url_video; ?></td>
				<td><?php echo $key->img_video; ?></td>
				<td><?php echo $key->orden; ?></td>
				<td><a href="<?php echo base_url('/videos/editar/'.$key->id_video); ?>" class="btn btn-sm btn-warning">Editar</a></td>
				<td><a href="<?php echo base_url('/videos/eliminar/'.$key->id_video); ?>" class="btn btn-sm btn-danger">Eliminar</a></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

</div>
</main>