<main>

<section id="hero_in" class="general">
  <div class="wrapper">
	<div class="container">
	  <h1 class="fadeInUp"><span></span>Videos</h1>
	</div>
  </div>
</section>
<div class="logo-empresa"><img src="<?php echo base_url(); ?>/img/logo-empresa.jpg" alt=""></div>

<div class="container margin_default">
	<div class="main_title_2">
		<span><em></em></span>
		<h2>Nuevo Video</h2>
		<p>Agregar nuevo video.</p>
	</div>
	
	<form action="<?php echo base_url('/videos/crear');?>" method="post">
		<div class="mb-3">
			<label for="id_tipo" class="form-label">Tipo de Usuario</label>
			<input type="text" class="form-control" id="id_tipo" name="id_tipo" required>
		</div>
		
		<div class="mb-3">
			<label for="titulo_video" class="form-label">Título</label>
			<input type="text" class="form-control" id="titulo_video" name="titulo_video" required>
		</div>
		
		<div class="mb-3">
			<label for="url_video" class="form-label">URL Video</label>
			<input type="text" class="form-control" id="url_video" name="url_video" required>
		</div>
		
		<div class="mb-3">
			<label for="img_video" class="form-label">Imagen</label>
			<input type="text" class="form-control" id="img_video" name="img_video" required>
		</div>
		
		<div class="mb-3">
			<label for="orden" class="form-label">Orden</label>
			<input type="text" class="form-control" id="orden" name="orden" required>
		</div>
	  
	  
	  <p class="text-center">
		  <button type="submit" class="btn_1 rounded">Crear Nuevo Video</button>&nbsp;&nbsp;<a href="<?php echo base_url('videos/listado'); ?>" class="btn_1 rounded" style="background:#cc0000;">Cancelar</a>
	  </p>
	</form>

</div>

</main>