<main>

<section id="hero_in" class="general">
  <div class="wrapper">
	<div class="container">
	  <h1 class="fadeInUp"><span></span>Empresas</h1>
	</div>
  </div>
</section>
<div class="logo-empresa"><img src="<?php echo base_url(); ?>/img/logo-empresa.jpg" alt=""></div>

<div class="container margin_default">
	
	<div class="main_title_2">
		<span><em></em></span>
		<h2>Empresas</h2>
		<p>Listado de empresas.</p>
	</div>
	
	
	<p class="text-center">
		<a href="<?php echo base_url('empresas/nuevo'); ?>" class="btn_1 rounded">Crear Nueva Empresa</a>&nbsp;&nbsp;<a href="#" class="btn_1 rounded">Carga Masiva</a>
	</p>
	
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th>Logo</th>
				<th>Nombre</th>
				<th>RUT</th>
				<th>Giro</th>
				<th>Trabajadores</th>
				<th>Editar</th>
				<th>Eliminar</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($datos as $key) : ?>
			<tr>
				<td style="text-align:center"><img src="<?php echo $key->img_empresa; ?>" class="rounded" width="30"></td>
				<td><?php echo $key->nombre_empresa; ?></td>
				<td><?php echo $key->rut_empresa; ?></td>
				<td><?php echo $key->giro_empresa; ?></td>
				<td><?php echo $key->cantidad_trabajadores; ?></td>
				<td><a href="<?php echo base_url('/empresas/editar/'.$key->id_empresa); ?>" class="btn btn-sm btn-warning">Editar</a></td>
				<td><a href="<?php echo base_url('/empresas/eliminar/'.$key->id_empresa); ?>" class="btn btn-sm btn-danger">Eliminar</a></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

</div>
</main>