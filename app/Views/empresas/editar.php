<main>

<section id="hero_in" class="general">
  <div class="wrapper">
	<div class="container">
	  <h1 class="fadeInUp"><span></span>Empresas</h1>
	</div>
  </div>
</section>
<div class="logo-empresa"><img src="<?php echo base_url(); ?>/img/logo-empresa.jpg" alt=""></div>

<div class="container margin_default">
	<div class="main_title_2">
		<span><em></em></span>
		<h2>Editar</h2>
		<p>Editar empresa.</p>
	</div>

	<form action="<?php echo base_url('/empresas/edita');?>" method="post">
	  <div class="mb-3">
		  <label for="nombre_empresa" class="form-label">Nombre</label>
		  <input type="text" class="form-control" id="nombre_empresa" name="nombre_empresa" value="<?php echo $datos[0]['nombre_empresa'];?>" required>
		</div>
		<div class="mb-3">
		  <label for="rut_empresa" class="form-label">RUT</label>
		  <input type="text" class="form-control" id="rut_empresa" name="rut_empresa" value="<?php echo $datos[0]['rut_empresa'];?>" required>
		</div>
		
		<div class="mb-3">
			<label for="giro_empresa" class="form-label">Giro</label>
			<input type="text" class="form-control" id="giro_empresa" name="giro_empresa" value="<?php echo $datos[0]['giro_empresa'];?>" required>
		</div>
		
		<div class="mb-3">
			  <label for="cantidad_trabajadores" class="form-label">Cantidad de Trabajadores</label>
			  <input type="number" class="form-control" id="cantidad_trabajadores" name="cantidad_trabajadores" value="<?php echo $datos[0]['cantidad_trabajadores'];?>" required>
		</div>
		
		<div class="mb-3">
			  <label for="img_empresa" class="form-label">Logo Empresa</label>
			  <input type="text" class="form-control" id="img_empresa" name="img_empresa" value="<?php echo $datos[0]['img_empresa'];?>" required>
		</div>
	  

	  
	  <input type="hidden" name="id_empresa" value="<?php echo $datos[0]['id_empresa'];?>">
	  <p class="text-center">
			<button type="submit" class="btn_1 rounded">Editar Empresa</button>&nbsp;&nbsp;<a href="<?php echo base_url('empresas/listado'); ?>" class="btn_1 rounded" style="background:#cc0000;">Cancelar</a>
		</p>
	</form>

</div>

</main>