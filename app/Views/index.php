<main>
	<section class="header-video">
		<div id="hero_video">
			<div>
				<h3><strong>Plataforma de seguridad laboral</strong><br>
			  para tus teletrabajadores</h3>
				<p>Revisa, administra y mejora la <strong>seguridad laboral</strong> de todos tus trabajadores que cumplen sus tareas desde casa.</p>
			</div>
			<a href="#first_section" class="btn_explore hidden_tablet"><i class="ti-arrow-down"></i></a>
		</div>
		<img src="img/video_fix.png" alt="" class="header-video--media" data-video-src="video/intro" data-teaser-source="video/intro" data-provider="" data-video-width="1920" data-video-height="960">
	</section>
	<!-- /header-video -->


	<div class="container margin_default" id="first_section">
		<div class="main_title_2">
			<span><em></em></span>
			<h2>¿Cómo funciona?</h2>
			<p>En 4 simples pasos:</p>
		</div>
		<div class="row how_home">
			<div class="col-lg-4">
				<figure>
					<img src="img/1.png" alt="" class="img-fluid">
				</figure>
			</div>
			<div class="col-lg-8">
				<ul>
					<li><strong>1</strong><h4>Regístrate e Inicia sesión</h4><p>Regístrate totalmente gratis e inicia sesión para acceder a la plataforma de administración.</p></li>
					<li><strong>2</strong>
				  <h4>Ingresa tus teletrabajadores</h4><p>Crea a tus trabajadores que estén en modalidad teletrabajo y les llegará automáticamente una invitación para acceder al sistema.</p></li>
					<li><strong>3</strong><h4>Acceso teletrabajadores</h4>
				  <p>Ellos deberán acceder y llenar una encuesta con su forma actual de trabajo. También podrán solicitar la asesoría y asistencia en vivo de una prevencionista de riesgo. </p></li>
					<li><strong>4</strong><h4>¡Todo listo!</h4><p>Ahora tendrás acceso a una matriz de riesgo, un plan de mejora, informes y estadísticas de todos tus trabajadores.</p></li>
				</ul>
				<p><a href="<?php echo base_url('/registrar'); ?>" class="btn_1 rounded">Comienza ahora</a><br></p>
				
		  </div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container --><!-- /bg_color_1 --><!-- /container --><!--/call_section--><!-- /bg_color_1 -->
</main>
<!-- /main -->