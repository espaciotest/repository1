<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {   
        echo view('common/head');
        echo view('common/header');
        echo view('index');
        echo view('common/footer'); 
        //return view('welcome_message');
    }
    
   
}
