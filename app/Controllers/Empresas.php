<?php

namespace App\Controllers;
use App\Models\EmpresasModel;

class Empresas extends BaseController
{
    public function index()
    {
        echo view('empresas/listado');
    }
    
    public function listado()
    {
        $result = session('result');
        
        $empresas = new EmpresasModel();
        $datos = $empresas->all();
        
        $data = [ "datos" => $datos, "result" => $result ];
        
        echo view('common/head');
        echo view('common/header');
        echo view('empresas/listado', $data);
        echo view('common/footer', $data);
    }
    
    public function nuevo() {
        
        echo view('common/head');
        echo view('common/header');
        echo view('empresas/nuevo');
        echo view('common/footer');
    }
    
    public function crear() {
        
        $data = [
            'nombre_empresa' 		     => $this->request->getPost('nombre_empresa'),
            'rut_empresa' 	             => $this->request->getPost('rut_empresa'),
            'giro_empresa' 		         => $this->request->getPost('giro_empresa'),
            'cantidad_trabajadores'      => $this->request->getPost('cantidad_trabajadores'),
            'img_empresa'                => $this->request->getPost('img_empresa'),
        ];
        
        $empresas = new EmpresasModel();
        $result = $empresas->_create($data);
        
        if ($result > 0) {
            return redirect()->to(base_url('/empresas/listado'))->with('result','1');
        } else {
            return redirect()->to(base_url('/empresas/listado'))->with('result','0');
        }

    }
    
    public function editar() {
        $request = \Config\Services::request();
        $id = $request->uri->getSegment(3);
        
        $data = ['id_empresa' => $id];
        
        $empresas = new EmpresasModel();
        $result = $empresas->one($data);
        
        $info = ['datos' => $result];
        echo view('common/head');
        echo view('common/header');
        echo view('empresas/editar',$info);
        echo view('common/footer');
        
    }
    
    public function edita() {
        
        $data = [
            'nombre_empresa'            => $this->request->getPost('nombre_empresa'),
            'rut_empresa'               => $this->request->getPost('rut_empresa'),
            'giro_empresa'              => $this->request->getPost('giro_empresa'),
            'cantidad_trabajadores'     => $this->request->getPost('cantidad_trabajadores'),
            'img_empresa'               => $this->request->getPost('img_empresa'),
        ];  
        
        $where = [
            'id_empresa' => $this->request->getPost('id_empresa'),
        ];
        
        $empresas = new EmpresasModel();
        $result = $empresas->_update($where, $data);
        
        if ($result) {
            return redirect()->to(base_url('/empresas/listado'))->with('result','1');
        } else {
            return redirect()->to(base_url('/empresas/listado'))->with('result','0');
        }
        
    }
    
    public function eliminar() {
        $request = \Config\Services::request();
        $id = $request->uri->getSegment(3);
        
        $data = ['id_empresa' => $id];
        $empresas = new EmpresasModel();
        $result = $empresas->_delete($data);
        
        if ($result) {
            return redirect()->to(base_url('/empresas/listado'))->with('result','1');
        } else {
            return redirect()->to(base_url('/empresas/listado'))->with('result','0');
        }
    }
    
}
