<?php

namespace App\Controllers;
use App\Models\TiposModel;

class Tipos extends BaseController
{
    public function index()
    {
        echo view('tipos/listado');
    }
    
    public function listado()
    {
        $result = session('result');
        
        $tipos = new TiposModel();
        $datos = $tipos->all();
        
        $data = [ "datos" => $datos, "result" => $result ];
        
        echo view('common/head');
        echo view('common/header');
        echo view('tipos/listado', $data);
        echo view('common/footer', $data);
    }
    
    public function nuevo() {
        
        echo view('common/head');
        echo view('common/header');
        echo view('tipos/nuevo');
        echo view('common/footer');
    }
    
    public function crear() {
        
        $data = [
            'tipo' 		     => $this->request->getPost('tipo'),
        ];
        
        $tipos = new TiposModel();
        $result = $tipos->_create($data);
        
        if ($result > 0) {
            return redirect()->to(base_url('/tipos/listado'))->with('result','1');
        } else {
            return redirect()->to(base_url('/tipos/listado'))->with('result','0');
        }

    }
    
    public function editar() {
        $request = \Config\Services::request();
        $id = $request->uri->getSegment(3);
        
        $data = ['id' => $id];
        
        $tipos = new TiposModel();
        $result = $tipos->one($data);
        
        $info = ['datos' => $result];
        echo view('common/head');
        echo view('common/header');
        echo view('tipos/editar',$info);
        echo view('common/footer');
        
    }
    
    public function edita() {
        
        $data = [
            'tipo'            => $this->request->getPost('tipo'),
        ];  
        
        $where = [
            'id' => $this->request->getPost('id'),
        ];
        
        $tipos = new TiposModel();
        $result = $tipos->_update($where, $data);
        
        if ($result) {
            return redirect()->to(base_url('/tipos/listado'))->with('result','1');
        } else {
            return redirect()->to(base_url('/tipos/listado'))->with('result','0');
        }
        
    }
    
    public function eliminar() {
        $request = \Config\Services::request();
        $id = $request->uri->getSegment(3);
        
        $data = ['id' => $id];
        $tipos = new TiposModel();
        $result = $tipos->_delete($data);
        
        if ($result) {
            return redirect()->to(base_url('/tipos/listado'))->with('result','1');
        } else {
            return redirect()->to(base_url('/tipos/listado'))->with('result','0');
        }
    }
    
}
