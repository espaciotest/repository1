<?php

namespace App\Controllers;
use App\Models\VideosModel;

class Videos extends BaseController
{
    public function index()
    {
        echo view('videos/listado');
    }
    
    public function listado()
    {
        $result = session('result');
        
        $tipos = new VideosModel();
        $datos = $tipos->all();
        
        $data = [ "datos" => $datos, "result" => $result ];
        
        echo view('common/head');
        echo view('common/header');
        echo view('videos/listado', $data);
        echo view('common/footer', $data);
    }
    
    public function nuevo() {
        
        echo view('common/head');
        echo view('common/header');
        echo view('videos/nuevo');
        echo view('common/footer');
    }
    
    public function crear() {
        
        $data = [
            'id_tipo'       => $this->request->getPost('id_tipo'),
            'titulo_video'  => $this->request->getPost('titulo_video'),
            'url_video'     => $this->request->getPost('url_video'),
            'img_video'     => $this->request->getPost('img_video'),
            'orden'         => $this->request->getPost('orden'),
        ];
        
        $tipos = new VideosModel();
        $result = $tipos->_create($data);
        
        if ($result > 0) {
            return redirect()->to(base_url('/videos/listado'))->with('result','1');
        } else {
            return redirect()->to(base_url('/videos/listado'))->with('result','0');
        }

    }
    
    public function editar() {
        $request = \Config\Services::request();
        $id = $request->uri->getSegment(3);
        
        $data = ['id_video' => $id];
        
        $tipos = new VideosModel();
        $result = $tipos->one($data);
        
        $info = ['datos' => $result];
        echo view('common/head');
        echo view('common/header');
        echo view('videos/editar',$info);
        echo view('common/footer');
        
    }
    
    public function edita() {
        
        $data = [
            'id_tipo'       => $this->request->getPost('id_tipo'),
            'titulo_video'  => $this->request->getPost('titulo_video'),
            'url_video'     => $this->request->getPost('url_video'),
            'img_video'     => $this->request->getPost('img_video'),
            'orden'         => $this->request->getPost('orden'),
        ];  
        
        $where = [
            'id_video' => $this->request->getPost('id_video'),
        ];
        
        $tipos = new VideosModel();
        $result = $tipos->_update($where, $data);
        
        if ($result) {
            return redirect()->to(base_url('/videos/listado'))->with('result','1');
        } else {
            return redirect()->to(base_url('/videos/listado'))->with('result','0');
        }
        
    }
    
    public function eliminar() {
        $request = \Config\Services::request();
        $id = $request->uri->getSegment(3);
        
        $data = ['id' => $id];
        $tipos = new VideosModel();
        $result = $tipos->_delete($data);
        
        if ($result) {
            return redirect()->to(base_url('/videos/listado'))->with('result','1');
        } else {
            return redirect()->to(base_url('/videos/listado'))->with('result','0');
        }
    }
    
}
