<?php

namespace App\Controllers;
use App\Models\UsuariosModel;

class Usuarios extends BaseController
{
    public function index()
    {   
        $result = session('result');
        $data = [ "result" => $result ];
        echo view('usuarios/login',$data);
    }
    
    public function login()
    {
        $rut = $this->request->getPost('rut');
        $clave = $this->request->getPost('clave');
        
        $usuario = new UsuariosModel();
        
        $datos = $usuario->one(['rut' => $rut]);
        
        if (count($datos) > 0 && password_verify($clave, $datos[0]['clave'])) {
            $data = [
                'rut' => $datos[0]['rut'],
                'tipo' => $datos[0]['tipo'],
            ];
            $session = session();
            $session->set($data);
            
            return redirect()->to(base_url('/usuarios/listado'))->with('mensaje','1');
        } else {
            return redirect()->to(base_url('/'))->with('mensaje','0');
        }
    }
    
    public function registrar() {
        $result = session('result');
        $data = [ "result" => $result ];
        echo view('usuarios/registrar', $data);
    }
    
    public function registro() {
        
        if ($this->request->getPost('clave') != $this->request->getPost('clave2')) {
            return redirect()->to(base_url('/usuarios/registrar'))->with('result','0');
        } else {
            $data = [
                'nombre'        => $this->request->getPost('nombre'),
                'apellido'      => $this->request->getPost('apellido'),
                'rut'           => $this->request->getPost('rut'),
                'clave'         => password_hash($this->request->getPost('clave'), PASSWORD_DEFAULT),
                'tipo'          => 5,
                'estado'        => 3,
            ];
        }
            
        $usuarios = new UsuariosModel();
        $result = $usuarios->_create($data);
        
        if ($result > 0) {
            return redirect()->to(base_url('/login'))->with('result','1');
        } else {
            return redirect()->to(base_url('/usuarios/registrar'))->with('result','0');
        }
    
        }
    
    public function logout() {
        $session = session();
        $session->destroy();
        return redirect()->to(base_url('/'));
    }
    
    public function listado()
    {
        $result = session('result');
        
        $usuarios = new UsuariosModel();
        $datos = $usuarios->all();
        
        $data = [ "datos" => $datos, "result" => $result ];
        
        echo view('common/head');
        echo view('common/header');
        echo view('usuarios/listado', $data);
        echo view('common/footer', $data);
    }
    
    public function nuevo() {
        
        echo view('common/head');
        echo view('common/header');
        echo view('usuarios/nuevo');
        echo view('common/footer');
    }
    
    public function crear() {
        
        $data = [
            'nombre'        => $this->request->getPost('nombre'),
            'apellido'      => $this->request->getPost('apellido'),
            'rut'           => $this->request->getPost('rut'),
            'email'         => $this->request->getPost('email'),
            'fono'          => $this->request->getPost('fono'),
            'empresa'       => $this->request->getPost('empresa'),
            'clave'         => password_hash($this->request->getPost('clave'), PASSWORD_DEFAULT),
            'tipo'          => $this->request->getPost('tipo'),
        ];
        
        $usuarios = new UsuariosModel();
        $result = $usuarios->_create($data);
        
        if ($result > 0) {
            return redirect()->to(base_url('/usuarios/listado'))->with('result','1');
        } else {
            return redirect()->to(base_url('/usuarios/listado'))->with('result','0');
        }

    }
    
    public function editar() {
        $request = \Config\Services::request();
        $id = $request->uri->getSegment(3);
        
        $data = ['id' => $id];
        
        $usuarios = new UsuariosModel();
        $result = $usuarios->one($data);
        
        $info = ['datos' => $result];
        echo view('common/head');
        echo view('common/header');
        echo view('usuarios/editar',$info);
        echo view('common/footer');
        
    }
    
    public function edita() {
        
        if ($this->request->getPost('clave') == NULL) {
            $data = [
                'nombre'    => $this->request->getPost('nombre'),
                'apellido'  => $this->request->getPost('apellido'),
                'rut'       => $this->request->getPost('rut'),
                'email'     => $this->request->getPost('email'),
                'fono'      => $this->request->getPost('fono'),
                'empresa'   => $this->request->getPost('empresa'),
                'clave' 	=> $this->request->getPost('clave_anterior'),
                'tipo' 		=> $this->request->getPost('tipo'),
            ];
        } else {
            $data = [
                'nombre'    => $this->request->getPost('nombre'),
                'apellido'  => $this->request->getPost('apellido'),
                'rut'       => $this->request->getPost('rut'),
                'email'     => $this->request->getPost('email'),
                'fono'      => $this->request->getPost('fono'),
                'empresa'   => $this->request->getPost('empresa'),
                'clave' 	=> password_hash($this->request->getPost('clave'), PASSWORD_DEFAULT),
                'tipo' 		=> $this->request->getPost('tipo'),
            ];
        }   
        
        $where = [
            'id' => $this->request->getPost('id'),
        ];
        $usuarios = new UsuariosModel();
        $result = $usuarios->_update($where, $data);
        
        if ($result) {
            return redirect()->to(base_url('/usuarios/listado'))->with('result','1');
        } else {
            return redirect()->to(base_url('/usuarios/listado'))->with('result','0');
        }
        
    }
    
    public function eliminar() {
        $request = \Config\Services::request();
        $id = $request->uri->getSegment(3);
        
        $data = ['id' => $id];
        $usuarios = new UsuariosModel();
        $result = $usuarios->_delete($data);
        
        if ($result) {
            return redirect()->to(base_url('/usuarios/listado'))->with('result','1');
        } else {
            return redirect()->to(base_url('/usuarios/listado'))->with('result','0');
        }
    }
    
}
